﻿using System.Drawing;
using System.IO;
using System.Windows.Media.Imaging;

namespace MS.PhotoViewer.Infrastructure.Tools
{
    public static class ByteArrayExtensions
    {
        public static Stream ToStream( this byte[] data, bool writable = false )
        {
            return new MemoryStream(data, writable);
        }

        public static BitmapImage ToCachedBitmapImage( this byte[] data, int decodePixelWidth = 0 )
        {
            if( data!=null )
            {
                using( Stream stream = data.ToStream() )
                {
                    BitmapImage bi = new BitmapImage();
                    bi.BeginInit();
                    bi.CacheOption=BitmapCacheOption.OnLoad;
                    bi.DecodePixelWidth=decodePixelWidth;
                    bi.StreamSource=stream;
                    bi.EndInit();
                    return bi;
                }
            }
            return null;
        }

        public static BitmapImage ToBitmapImage( this byte[] data, int decodePixelWidth = 0 )
        {
            if( data!=null )
            {
                BitmapImage bi = new BitmapImage();
                bi.BeginInit();
                bi.CacheOption=BitmapCacheOption.None;
                bi.DecodePixelWidth=decodePixelWidth;
                bi.StreamSource=data.ToStream();
                bi.EndInit();
                return bi;
            }
            return null;
        }

        public static Bitmap ToBitmap( this byte[] data )
        {
            using( Stream stream = data?.ToStream() )
            {
                return stream!=null ? new Bitmap(stream) : null;
            }
        }
    }
}
