﻿using ImageProcessor;
using System.IO;

namespace MS.PhotoViewer.Infrastructure.Services
{
    public interface IPhotoRepository
    {
        string Name { get; }

        string Current { get; }

        void PreviousItem();

        void NextItem();

        void Save(ImageFactory imageFactory, FileStream outStream);
    }
}
