﻿using MS.PhotoViewer.Repository;
using PhotoViewer.CenterModule;
using PhotoViewer.LowerModule;
using PhotoViewer.UpperModule;
using Prism.Ioc;
using Prism.Modularity;
using Prism.Unity;
using System.Windows;

namespace MS.PhotoViewer
{
    public partial class App : PrismApplication
    {
        protected override Window CreateShell()
        {
            return Container.Resolve<MainWindow>();
        }

        protected override void RegisterTypes( IContainerRegistry containerRegistry )
        {
        }

        protected override void ConfigureModuleCatalog( IModuleCatalog moduleCatalog )
        {
            base.ConfigureModuleCatalog(moduleCatalog);

            moduleCatalog.AddModule<RepositoryModule>();
            moduleCatalog.AddModule<Commands>();
            moduleCatalog.AddModule<CenterModulee>();
            moduleCatalog.AddModule<Navigation>();
        }
    }
}
