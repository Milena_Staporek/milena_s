﻿using MS.PhotoViewer.CenterModule.ViewModel;
using System.Windows.Controls;

namespace MS.PhotoViewer.CenterModule.View
{
    public partial class CenterView : UserControl
    {
        public CenterView( CenterViewModel viewModel )
        {
            InitializeComponent();
            Loaded+=( s, e ) => DataContext=viewModel;
        }
    }
}
