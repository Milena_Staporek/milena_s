﻿using MS.PhotoViewer.Infrastructure.Services;
using Prism.Mvvm;
using Prism.Events;
using MS.PhotoViewer.Infrastructure;
using System.IO;
using MS.PhotoViewer.Infrastructure.Tools;
using ImageProcessor;

namespace MS.PhotoViewer.CenterModule.ViewModel
{
    public class CenterViewModel : BindableBase
    {
        private readonly IPhotoRepository _photoRepository;

        public CenterViewModel(IPhotoRepository photoRepository, IEventAggregator eventAggregator)
        {
            _photoRepository = photoRepository;

            eventAggregator.GetEvent<EventNext>().Subscribe(HandleEventNext);
            eventAggregator.GetEvent<EventPrevious>().Subscribe(HandleEventPrevious);
            eventAggregator.GetEvent<EventRotate>().Subscribe(HandleEventRotate);
            eventAggregator.GetEvent<EventZoom>().Subscribe(HandleEventZoom);
        }

        private void HandleEventRotate(int value)
        {
            _rotateValue += value;
            RaisePropertyChanged(nameof(RotateValue));

            byte[] photoBytes = File.ReadAllBytes(PhotoPath);

            using (MemoryStream inStream = new MemoryStream(photoBytes))
            {
                using (FileStream outStream = File.OpenWrite(PhotoPath))
                {
                    using (ImageFactory imageFactory = new ImageFactory(preserveExifData: true))
                    {
                        imageFactory.Load(inStream)
                                    .Rotate(value);


                        _photoRepository.Save(imageFactory, outStream);

                    }
                }               
            }
        }

        public string PhotoPath => _photoRepository.Current;

        private float _rotateValue;
        public float RotateValue
        {
            get
            {
                return _rotateValue;
            }
            set
            {
                RaisePropertyChanged(nameof(RotateValue));
            }
        }

        private float _value;
        public float ZoomValue
        {
            get
            {
                return _value;
            }
            set
            {
                RaisePropertyChanged(nameof(ZoomValue));
            }
        }

        private void HandleEventZoom(int value)
        {
            _value = ((float)value/100) + 1;
            RaisePropertyChanged(nameof(ZoomValue));
        }

        private void HandleEventNext()
        {
            _photoRepository.NextItem();
            _rotateValue = 0;
            RaisePropertyChanged(nameof(RotateValue));
            RaisePropertyChanged(nameof(PhotoPath));
        }

        private void HandleEventPrevious()
        {
            _photoRepository.PreviousItem();
            _rotateValue = 0;
            RaisePropertyChanged(nameof(RotateValue));
            RaisePropertyChanged(nameof(PhotoPath));
        }
    }
}
