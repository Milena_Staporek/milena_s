﻿using MS.PhotoViewer.CenterModule.View;
using Prism.Ioc;
using Prism.Modularity;
using Prism.Regions;

namespace PhotoViewer.CenterModule
{
    public class CenterModulee : IModule
    {
        public void OnInitialized( IContainerProvider containerProvider )
        {
            var regionManager = containerProvider.Resolve<IRegionManager>();
            regionManager.RegisterViewWithRegion("MainRegion", typeof(CenterView));
        }

        public void RegisterTypes( IContainerRegistry containerRegistry )
        {
        }
    }
}
