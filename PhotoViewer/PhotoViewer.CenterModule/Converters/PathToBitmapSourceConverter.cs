﻿using MS.PhotoViewer.Infrastructure.Tools;
using System;
using System.Globalization;
using System.IO;
using System.Windows.Data;

namespace MS.PhotoViewer.CenterModule.Converters
{
    class PathToBitmapSourceConverter : IValueConverter
    {
        public object Convert( object value, Type targetType, object parameter, CultureInfo culture )
        {
            if( value is string path ) 
            {
                return File.ReadAllBytes(path).ToBitmapImage();
            }
            return value;
        }

        public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture )
        {
            throw new NotImplementedException();
        }
    }
}
