﻿using MS.PhotoViewer.LowerModule.View;
using Prism.Ioc;
using Prism.Modularity;
using Prism.Regions;

namespace PhotoViewer.LowerModule
{
    public class Navigation : IModule
    {
        public void OnInitialized( IContainerProvider containerProvider )
        {
            var regionManager = containerProvider.Resolve<IRegionManager>();
            regionManager.RegisterViewWithRegion("BottomRegion", typeof(LowerView));
        }

        public void RegisterTypes( IContainerRegistry containerRegistry )
        {
        }
    }
}
