﻿using System.Windows.Input;
using MS.PhotoViewer.Infrastructure;
using Prism.Commands;
using Prism.Mvvm;

namespace MS.PhotoViewer.LowerModule.ViewModel
{
    public class LowerViewModel : BindableBase
    {
        private Prism.Events.IEventAggregator _eventAggregator;

        public ICommand ButtonCommandNext
        {
            get
            {
                return new DelegateCommand<object>(PublishEventNext);
            }
        }

        public ICommand ButtonCommanPrevious
        {
            get
            {
                return new DelegateCommand<object>(PublishEventPrevious);
            }
        }

        public LowerViewModel(Prism.Events.IEventAggregator eventAggregator)
        {
            _eventAggregator = eventAggregator;
        }

        private void PublishEventPrevious(object context)
        {
            _eventAggregator
              .GetEvent<EventPrevious>()
              .Publish();
        }

        private void PublishEventNext(object context)
        {
            _eventAggregator
              .GetEvent<EventNext>()
              .Publish();
        }

    }
}
