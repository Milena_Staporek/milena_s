﻿using MS.PhotoViewer.LowerModule.ViewModel;
using System.Windows.Controls;

namespace MS.PhotoViewer.LowerModule.View
{
    public partial class LowerView : UserControl
    {
        public LowerView(LowerViewModel viewModel)
        {
            InitializeComponent();
            Loaded+=( s, e ) => DataContext=viewModel;
        }
    }
}
