﻿using MS.PhotoViewer.UpperModule.ViewModel;
using System.Windows.Controls;

namespace MS.PhotoViewer.UpperModule.View
{
    public partial class UpperView : UserControl
    {
        public UpperView( UpperViewModel viewModel )
        {
            InitializeComponent();
            Loaded+=( s, e ) => DataContext=viewModel;
        }
    }
}
