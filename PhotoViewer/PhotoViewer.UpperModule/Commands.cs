﻿using MS.PhotoViewer.UpperModule.View;
using Prism.Ioc;
using Prism.Modularity;
using Prism.Regions;

namespace PhotoViewer.UpperModule
{
    public class Commands : IModule
    {
        public void OnInitialized( IContainerProvider containerProvider )
        {
            var regionManager = containerProvider.Resolve<IRegionManager>();
            regionManager.RegisterViewWithRegion("RibbonRegion", typeof(UpperView));
        }

        public void RegisterTypes( IContainerRegistry containerRegistry )
        {
        }
    }
}
