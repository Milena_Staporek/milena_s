﻿using MS.PhotoViewer.Infrastructure;
using Prism.Commands;
using Prism.Mvvm;
using System.Windows.Input;

namespace MS.PhotoViewer.UpperModule.ViewModel
{
    public class UpperViewModel : BindableBase
    {
        private Prism.Events.IEventAggregator _eventAggregator;

        private int _value;
        public int Value
        {
            get
            {
                _eventAggregator
             .GetEvent<EventZoom>()
             .Publish(_value);
                return _value;
            }
            set
            {
                if (_value != value)
                {
                    _value = value;
                    RaisePropertyChanged(nameof(Value));
                }
            }
        }

        public ICommand ButtonCommandRotate
        {
            get
            {
                return new DelegateCommand<object>(PublishEventRotate);
            }
        }

        public UpperViewModel(Prism.Events.IEventAggregator eventAggregator)
        {
            _eventAggregator = eventAggregator;
        }

        private void PublishEventRotate(object context)
        {
            _eventAggregator
              .GetEvent<EventRotate>()
              .Publish(90);
        }
    }
}
