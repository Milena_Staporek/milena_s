﻿using MS.PhotoViewer.Infrastructure.Services;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using ImageProcessor;

namespace MS.PhotoViewer.Repository
{
    class DirectoryPhotoRepository : IPhotoRepository
    {
        private readonly string _directory;
        private readonly string[] _allowedExtensions;

        private List<string> _imagesPaths;

        public DirectoryPhotoRepository( string directory )
        {
            _directory=directory;
            Name=$"{nameof(DirectoryPhotoRepository)} : {Path.GetDirectoryName(_directory)}";
            _allowedExtensions=ImageCodecInfo.GetImageEncoders().SelectMany(info => info.FilenameExtension.Split(';').Select(ext=>ext.TrimStart('*').ToLower())).ToArray();

            LoadImagesPaths();
        }

        private void LoadImagesPaths()
        {
            _imagesPaths=Directory.GetFiles(_directory).Where(path => _allowedExtensions.Contains(Path.GetExtension(path).ToLower())).ToList();
            Current=_imagesPaths?.Any()??false ? _imagesPaths[0] : null;
        }

        public string Name { get; }

        public string Current
        {
            get;
            private set;
        }

        public void NextItem()
        {
            var idx = _imagesPaths.IndexOf(Current);
            Current=_imagesPaths.ElementAt(idx<_imagesPaths.Count-1?idx+1:0);
        }

        public void PreviousItem()
        {
            var idx = _imagesPaths.IndexOf(Current);
            Current=_imagesPaths.ElementAt(idx>0 ? idx-1 : _imagesPaths.Count-1);
        }

        public void Save(ImageFactory imageFactory, FileStream outStream)
        {
            imageFactory.Save(outStream);
        }
    }
}
