﻿using MS.PhotoViewer.Infrastructure.Services;
using Prism.Ioc;
using Prism.Modularity;
using System;

namespace MS.PhotoViewer.Repository
{
    public class RepositoryModule : IModule
    {
        public void OnInitialized( IContainerProvider containerProvider )
        {
        }

        public void RegisterTypes( IContainerRegistry containerRegistry )
        {
            containerRegistry.RegisterInstance<IPhotoRepository>(new DirectoryPhotoRepository(Environment.GetFolderPath(Environment.SpecialFolder.MyPictures)));
        }
    }
}
